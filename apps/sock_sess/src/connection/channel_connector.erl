-module(channel_connector).

-include_lib("public_key/include/public_key.hrl").
-include_lib("protocol/Protocol.hrl").

-define(RECEIVE_TIMEOUT, 3000).

-export([start/3, open_channel/3]).

%%%============================================================================
start(SessionManager, TCP_Socket, RSA_Key) ->
  {ok, erlang:spawn_link(?MODULE, open_channel, [SessionManager, TCP_Socket, RSA_Key])}.

%%%============================================================================
open_channel(Manager, TCP_Socket, RSA_Key) ->
  case gen_tcp:recv(TCP_Socket, 0, ?RECEIVE_TIMEOUT) of
    {ok, Data} ->
      MessageData = public_key:decrypt_private(Data, RSA_Key),
      case 'Protocol':decode('ChannelConnectionReq', MessageData) of
        {ok, ChannelConnectionReq} ->
          SessionID = ChannelConnectionReq#'ChannelConnectionReq'.session,
          gen_server:cast(Manager, {open_channel, TCP_Socket, SessionID});

        {error, _Reason} ->
          gen_tcp:close(TCP_Socket),
          lager:error("Connector failed: Invalid <ChannelOpenReq> request")
       end;

    {error, timeout} ->
      gen_tcp:close(TCP_Socket),
      lager:error("Open channel failed: timeout");

    {error, Reason} ->
      gen_tcp:close(TCP_Socket),
      lager:error("Open channel failed: ~p", [Reason])
  end.

%%%============================================================================
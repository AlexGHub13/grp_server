-module(sock_sess_connect_sup).
-behaviour(supervisor).

-export([start_link/1, init/1]).

-define(MAX_RESTART, 1).
-define(MAX_RESTART_TIME, 5000).
-define(SHATDOWN_TIME, 3000).

%%============================================================================
start_link(SessionID) ->
  supervisor:start_link(?MODULE, SessionID).

init(SessionID) ->
  {ok, {{one_for_all, ?MAX_RESTART, ?MAX_RESTART_TIME},
    [ {connect_controller,
        {connect_controller, start_link, [self(), SessionID]},
        temporary, ?SHATDOWN_TIME, worker, [connect_controller]} ]
  }}.

%%%============================================================================
